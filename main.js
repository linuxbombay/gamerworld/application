// Modules to control application life and create native browser window
const {app, BrowserWindow, shell} = require('electron')
const contextMenu = require('electron-context-menu');
const path = require('path')

contextMenu({
	prepend: (defaultActions, params, browserWindow, dictionarySuggestions) => [
		{
			label: 'Search Google for “{selection}”',
			// Only show it when right-clicking text
			visible: params.selectionText.trim().length > 0,
			click: () => {
				shell.openExternal(`https://google.com/search?q=${encodeURIComponent(params.selectionText)}`);
			}
		}
	]
});

//Vsync off by default (Turn on for lower end devices)
app.commandLine.appendSwitch('disable-frame-rate-limit')
app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required');

//FPS COUNTER (Only enabled for testing)
//app.commandLine.appendSwitch('show-fps-counter')

//Fixes WebGL support on old GPUS that get false unsupported message.
app.commandLine.appendSwitch('ignore-gpu-blacklist', 'true');

//DISABLED as it causes issues with lower end hardware, doesn't cause any issues being off
//app.commandLine.appendSwitch('enable-native-gpu-memory-buffers')

function createWindow () {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1350,
    height: 900,
    icon: __dirname + '/icon.png',
    backgroundColor: '#1D2127',
    webPreferences: {
      contextIsolation: true,
      enableRemoteModule: false,
      preload: path.join(__dirname, 'preload.js')
    }
  })

  // and load the index.html of the app.
  mainWindow.loadFile('splash.html')
  setTimeout(function () {
    mainWindow.loadURL('https://gw-ui.netlify.app');
  }, 3000) // Load store page after 3 secs
  mainWindow.maximize() // start maximized
  mainWindow.setMenuBarVisibility(false)
  mainWindow.setMenu(null)
  mainWindow.show();

  // Open the DevTools.
  //mainWindow.webContents.openDevTools()
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})
